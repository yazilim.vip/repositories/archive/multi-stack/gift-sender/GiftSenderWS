package vip.yazilim.giftsenderws;

import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class LambdaExample {
    public static void main(String[] args) {
		/*
		T = Person
		U = SummaryDto
		Function <Person, SummaryDto>
		TODO Research @FunctionalInterface
		 */

        Consumer<Integer> numberPrinter;
        numberPrinter = new Consumer<Integer>() {
            @Override
            public void accept(Integer x) {
                System.out.println("Consumer value : " + x);
            }
        };
        numberPrinter = x -> System.out.println("Consumer value : " + x);
        numberPrinter = x -> {
            System.out.println("Consumer value : " + x);
        };

        //---------------------------------------------------------------------------

        Supplier<Integer> randomNumberGiver;
        randomNumberGiver = new Supplier<Integer>() {
            @Override
            public Integer get() {
                return new Random().nextInt();
            }
        };
        randomNumberGiver = () -> new Random().nextInt();

        //---------------------------------------------------------------------------

        Function<Integer,String> numberIncrementor;
        numberIncrementor = new Function<Integer, String>() {
            @Override
            public String apply(Integer x) {
                return String.valueOf(++x);
            }
        };
        numberIncrementor = x -> String.valueOf(++x);

        //---------------------------------------------------------------------------

        Runnable myExecuter;
        myExecuter = new Runnable() {
            @Override
            public void run() {
                System.out.println("Hi, iam runnable.");
            }
        };
        myExecuter = () -> System.out.println("Hi, iam runnable.");

        //---------------------------------------------------------------------------

        var val1 = randomNumberGiver.get();
        numberPrinter.accept(val1);
        var res1 = numberIncrementor.apply(val1);
        myExecuter.run();

    }
}
