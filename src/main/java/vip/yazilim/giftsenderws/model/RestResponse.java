package vip.yazilim.giftsenderws.model;
import lombok.*;
import lombok.experimental.SuperBuilder;
/**
 * @author Burak Erkan - burak@yazilim.vip
 * 11 February 2022
 */

@AllArgsConstructor
@Getter @Setter
public class RestResponse<T> {
    private String message;
    private boolean success;
    private T data;

    @Builder
    public RestResponse(String message, T data, boolean success) {
        this.message = message;
        this.data = data;
        this.success = success;
    }

    /*
    public static ResponseModel<String> success(String data) {
        ResponseModel<String> responseModel = new ResponseModel<>();
        responseModel.setMessage("Success");
        responseModel.setSuccess(true);
        responseModel.setData(data);

        return responseModel;
    }
    public static ResponseModel<Integer> success2(Integer data) {
        ResponseModel<Integer> responseModel = new ResponseModel<>();
        responseModel.setMessage("Success");
        responseModel.setSuccess(true);
        responseModel.setData(data);

        return responseModel;
    }
    */

    public static <M> RestResponse<M> success(M data) {
        RestResponse<M> response = RestResponse.<M>builder()
                        .message("Success")
                        .data(data)
                        .success(true)
                        .build();
        return response;
    }

    public static <M> RestResponse<M> empty() {
        return success(null);
    }

    public static <M> RestResponse<M> error(M data) {
        RestResponse<M> response = RestResponse.<M>builder()
                .message("Error")
                .data(data)
                .success(false)
                .build();
        return response;
    }
}
