package vip.yazilim.giftsenderws;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import vip.yazilim.giftsenderws.config.PropertiesExample;
import vip.yazilim.giftsenderws.entity.enums.LevelType;

import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class GiftSenderWsApplication implements CommandLineRunner {

	/**
	 * GETTING PROPERTY WAY1
	 */
	private final PropertiesExample propertiesExample;

	/**
	 * GETTING PROPERTY WAY2 - Using System Environment Object
	 */
	private final Environment env;

	/**
	 * GETTING PROPERTY WAY3
	 */
	@Value("${spring.jpa.generate-ddl}")
	private String springJpaGenerateDdl;

	/**
	 * CUSTOM PROPERTY WAY
	 */
	@Value("${vip-properties.url:wwwyazilimvip}")
	private String vipUrl;

	@Override
	public void run(String... args) {
		log.info("\n\n");

		// Getting application.yml properties by using @ConfigurationProperties annotated Class
		log.info("GETTING PROPERTY WAY1 | {}",propertiesExample);

		// Getting application.yml property by using Environment Class
		String banner = env.getProperty("spring.main.banner-mode");
		log.info("GETTING PROPERTY WAY2 | Banner-Mode : {}", banner);

		//Getting application.yml property by using @Value annotation
		log.info("GETTING PROPERTY WAY3 | spring.jpa.generate-ddl : {}",springJpaGenerateDdl);

		//Getting propery which injected by @Value
		log.info("GETTING PROPERTY | vip url : ",vipUrl);

		log.info("\n\n");

	}

	public static void main(String[] args) {
		SpringApplication.run(GiftSenderWsApplication.class, args);
	}
}
