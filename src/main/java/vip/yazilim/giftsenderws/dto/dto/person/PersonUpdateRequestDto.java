package vip.yazilim.giftsenderws.dto.dto.person;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import vip.yazilim.giftsenderws.entity.enums.GenderType;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * @author Burak Erkan - 17 January 2022
 * @contact burak@yazilim.vip
 */

@SuperBuilder
@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
@EqualsAndHashCode
@ToString
public class PersonUpdateRequestDto {
	@NotEmpty(message = "name is mandatory")
	@Size(min = 2)
	private String name;

	@NotEmpty(message = "surname is mandatory")
	@Size(min = 2)
	private String surname;

	@NotNull
	private GenderType genderType;

	@NotEmpty(message = "birthPlace is mandatory")
	@Size(min = 2)
	private String birthPlace;

	@NotNull
	private LocalDate birthDate;

	@NotNull
	private LocalTime birthTime;

	@NotNull
	private boolean wantPresent;
}