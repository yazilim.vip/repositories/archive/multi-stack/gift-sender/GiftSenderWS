package vip.yazilim.giftsenderws.dto.dto.pricetype;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import vip.yazilim.giftsenderws.entity.enums.LevelType;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 01 February 2022
 */
@SuperBuilder
@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
@EqualsAndHashCode @ToString
public class PriceTypeSummaryDto {
	private LevelType levelType;
	private String levelName;
	private BigDecimal upperLimit;
	private BigDecimal lowerLimit;
}
