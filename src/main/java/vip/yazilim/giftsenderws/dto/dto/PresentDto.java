package vip.yazilim.giftsenderws.dto.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

/**
 * @author Burak Erkan - 17 January 2022
 * @contact burak@yazilim.vip
 */
@SuperBuilder
@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
@EqualsAndHashCode
@ToString
public class PresentDto {
	private Long id;
	private String name;
	private BigDecimal price;
	private Long categoryId;
}
