package vip.yazilim.giftsenderws.dto.dto.present;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 31 January 2022
 */
@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
@EqualsAndHashCode @ToString
@Builder
public class PresentSummaryDto {
	private Long id;
	private String name;
	private BigDecimal price;
	private Long categoryId;
}
