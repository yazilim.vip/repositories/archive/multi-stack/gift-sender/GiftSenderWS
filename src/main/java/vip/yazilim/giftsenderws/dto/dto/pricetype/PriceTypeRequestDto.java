package vip.yazilim.giftsenderws.dto.dto.pricetype;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import vip.yazilim.giftsenderws.entity.enums.LevelType;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 01 February 2022
 */
@SuperBuilder
@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
@EqualsAndHashCode @ToString
public class PriceTypeRequestDto {
	@NotNull
	@Size(min = 2)
	private String levelName;

	@NotNull
	private BigDecimal upperLimit;

	@NotNull
	@Min(0)
	private BigDecimal lowerLimit;
}
