package vip.yazilim.giftsenderws.dto.dto.category;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 26 January 2022
 */
@SuperBuilder
@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
@EqualsAndHashCode
@ToString
public class CategoryRequestDto {
	@NotNull
	private Long priceTypeId;
}
