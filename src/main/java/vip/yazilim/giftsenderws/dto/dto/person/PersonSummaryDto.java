package vip.yazilim.giftsenderws.dto.dto.person;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import vip.yazilim.giftsenderws.entity.enums.GenderType;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * @author Burak Erkan - 17 January 2022
 * @contact burak@yazilim.vip
 */

@SuperBuilder
@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
@EqualsAndHashCode
@ToString
public class PersonSummaryDto {
	private Long id;
	private String name;
	private String surname;
	private String mail;
	private GenderType genderType;
	private String birthPlace;
	private LocalDate birthDate;
	private LocalTime birthTime;
	private boolean wantPresent;
}