package vip.yazilim.giftsenderws.dto.dto.category;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 26 January 2022
 */
@SuperBuilder
@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
@EqualsAndHashCode
@ToString
public class CategorySummaryDto {
	private Long id;
	private Long priceTypeId;
}
