package vip.yazilim.giftsenderws.dto.dto.present;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 31 January 2022
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
@Builder
public class PresentRequestDto {
	@NotEmpty
	@Size(min = 2)
	private String name;

	@NotNull
	@Min(0)
	private BigDecimal price;

	@NotNull
	private Long categoryId;
}
