package vip.yazilim.giftsenderws.dto.mapper.examples;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import vip.yazilim.giftsenderws.entity.Person;
import vip.yazilim.giftsenderws.entity.enums.GenderType;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 21 January 2022
 *
 * Usage:
 * personRepo.save(personDto.toEntity());
 */

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class PersonDtoByMethod {
	private Long id;
	private String name;
	private String surname;
	private String mail;
	private GenderType genderType;
	private String birthPlace;
	private LocalDate birthDate;
	private LocalTime birthTime;
	private boolean wantPresent;

	public Person toEntity() {
		Person personEntity = new Person();
		personEntity.setId(this.id);
		personEntity.setName(this.name);
		personEntity.setSurname(this.surname);
		personEntity.setMail(this.mail);
		personEntity.setGenderType(this.genderType);
		personEntity.setBirthPlace(this.birthPlace);
		personEntity.setBirthDate(this.birthDate);
		personEntity.setBirthTime(this.birthTime);
		personEntity.setWantPresent(this.wantPresent);
		return personEntity;
	}
}