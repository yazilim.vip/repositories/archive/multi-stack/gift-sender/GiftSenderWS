package vip.yazilim.giftsenderws.dto.mapper.examples;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import vip.yazilim.giftsenderws.entity.Person;
import vip.yazilim.giftsenderws.entity.enums.GenderType;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 21 January 2022
 *
 * Usage:
 * Person person = personRepo.findAllById("123L");
 * PersonDto personDto = new PersonDto(person);
 */

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class PersonDtoByConstructor {
	private Long id;
	private String name;
	private String surname;
	private String mail;
	private GenderType genderType;
	private String birthPlace;
	private LocalDate birthDate;
	private LocalTime birthTime;
	private boolean wantPresent;

	public PersonDtoByConstructor(Person personEntity) {
		this.id = personEntity.getId();
		this.name = personEntity.getName();
		this.surname = personEntity.getSurname();
		this.mail = personEntity.getMail();
		this.genderType = personEntity.getGenderType();
		this.birthPlace = personEntity.getBirthPlace();
		this.birthDate = personEntity.getBirthDate();
		this.birthTime = personEntity.getBirthTime();
		this.wantPresent = personEntity.isWantPresent();
	}
}