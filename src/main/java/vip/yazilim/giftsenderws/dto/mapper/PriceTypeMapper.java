package vip.yazilim.giftsenderws.dto.mapper;

import org.mapstruct.Mapper;
import vip.yazilim.giftsenderws.dto.dto.pricetype.PriceTypeRequestDto;
import vip.yazilim.giftsenderws.dto.dto.pricetype.PriceTypeSummaryDto;
import vip.yazilim.giftsenderws.entity.PriceType;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 26 January 2022
 */
@Mapper(componentModel = "spring")
public interface PriceTypeMapper {
	PriceType toEntity(PriceTypeSummaryDto dto);
	PriceType toEntity(PriceTypeRequestDto dto);

	PriceTypeSummaryDto toPriceTypeSummaryDto(PriceType priceType);
	PriceTypeRequestDto toPriceTypeRequestDto(PriceType priceType);
}
