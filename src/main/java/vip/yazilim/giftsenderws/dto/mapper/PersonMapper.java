package vip.yazilim.giftsenderws.dto.mapper;

import org.springframework.stereotype.Component;
import vip.yazilim.giftsenderws.dto.dto.person.PersonCreateRequestDto;
import vip.yazilim.giftsenderws.dto.dto.person.PersonSummaryDto;
import vip.yazilim.giftsenderws.dto.dto.person.PersonUpdateRequestDto;
import vip.yazilim.giftsenderws.entity.Person;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 21 January 2022
 */
@Component
public class PersonMapper {

	public Person toEntity(PersonSummaryDto dto) {
		return Person.builder()
				.name(dto.getName())
				.surname(dto.getSurname())
				.mail(dto.getMail())
				.genderType(dto.getGenderType())
				.birthPlace(dto.getBirthPlace())
				.birthDate(dto.getBirthDate())
				.birthTime(dto.getBirthTime())
				.wantPresent(dto.isWantPresent())
				.build();
	}

	public Person toEntity(PersonUpdateRequestDto dto) {
		return Person.builder()
				.name(dto.getName())
				.surname(dto.getSurname())
				.genderType(dto.getGenderType())
				.birthPlace(dto.getBirthPlace())
				.birthDate(dto.getBirthDate())
				.birthTime(dto.getBirthTime())
				.wantPresent(dto.isWantPresent())
				.build();
	}

	public Person toEntity(PersonCreateRequestDto dto) {
		return Person.builder()
				.name(dto.getName())
				.surname(dto.getSurname())
				.mail(dto.getMail())
				.genderType(dto.getGenderType())
				.birthPlace(dto.getBirthPlace())
				.birthDate(dto.getBirthDate())
				.birthTime(dto.getBirthTime())
				.build();
	}

	public PersonSummaryDto toPersonSummaryDto(Person personEntity) {
		return PersonSummaryDto.builder()
				.id(personEntity.getId())
				.name(personEntity.getName())
				.surname(personEntity.getSurname())
				.mail(personEntity.getMail())
				.genderType(personEntity.getGenderType())
				.birthPlace(personEntity.getBirthPlace())
				.birthDate(personEntity.getBirthDate())
				.birthTime(personEntity.getBirthTime())
				.wantPresent(personEntity.isWantPresent())
				.build();
	}

}
