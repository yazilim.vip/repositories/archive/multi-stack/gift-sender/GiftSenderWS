package vip.yazilim.giftsenderws.dto.mapper;

import org.mapstruct.Mapper;
import vip.yazilim.giftsenderws.dto.dto.category.CategorySummaryDto;
import vip.yazilim.giftsenderws.dto.dto.category.CategoryRequestDto;
import vip.yazilim.giftsenderws.entity.Category;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 26 January 2022
 */
@Mapper(componentModel = "spring")
public interface CategoryMapper {
	Category toEntity(CategorySummaryDto dto);
	Category toEntity(CategoryRequestDto dto);

	CategorySummaryDto toCategorySummaryDto(Category category);
	CategoryRequestDto toCategoryRequestDto(Category category);
}
