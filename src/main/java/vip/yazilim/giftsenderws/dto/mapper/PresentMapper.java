package vip.yazilim.giftsenderws.dto.mapper;

import org.mapstruct.Mapper;
import vip.yazilim.giftsenderws.dto.dto.present.PresentRequestDto;
import vip.yazilim.giftsenderws.dto.dto.present.PresentSummaryDto;
import vip.yazilim.giftsenderws.entity.Present;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 26 January 2022
 */
@Mapper(componentModel = "spring")
public interface PresentMapper {
	Present toEntity(PresentSummaryDto dto);
	Present toEntity(PresentRequestDto dto);

	PresentSummaryDto toPresentSummaryDto(Present present);
	PresentRequestDto toPresentRequestDto(Present present);
}
