package vip.yazilim.giftsenderws.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vip.yazilim.giftsenderws.entity.Category;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 18 January 2022
 */
@Repository
public interface CategoryRepo extends JpaRepository<Category,Long> {
}
