package vip.yazilim.giftsenderws.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vip.yazilim.giftsenderws.entity.PriceType;
import vip.yazilim.giftsenderws.entity.enums.LevelType;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 18 January 2022
 */
@Repository
public interface PriceTypeRepo extends JpaRepository<PriceType, LevelType> {
}
