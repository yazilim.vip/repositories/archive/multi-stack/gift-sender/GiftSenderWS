package vip.yazilim.giftsenderws.config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import java.nio.charset.StandardCharsets;
import java.util.Locale;
/**
 * @author Burak Erkan, burak@yazilim.vip
 * 20.02.2022
 */

@Configuration
public class I18nConfig {

    @Bean
    public LocaleResolver myDefaultLocaleResolver() {
        var incomingLocaleHeaderResolver = new AcceptHeaderLocaleResolver();
        var defaultLocale = new Locale("tr", "TR");
        incomingLocaleHeaderResolver.setDefaultLocale(defaultLocale);
        return incomingLocaleHeaderResolver;
    }

    @Bean
    public ResourceBundleMessageSource messageSource() {
        var messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("messages");
        messageSource.setDefaultEncoding(StandardCharsets.UTF_8.name());
        return messageSource;
    }

    @Bean
    public LocalValidatorFactoryBean getValidator() {
        var bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(messageSource());
        return bean;
    }
}
