package vip.yazilim.giftsenderws.config;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author Burak Erkan, burak@yazilim.vip
 * 17.02.2022
 */
public class Messages {
    public static String getMessageForLocale(String messageKey, Locale locale) {
        return ResourceBundle
                .getBundle("messages", locale)
                .getString(messageKey);
    }
}
