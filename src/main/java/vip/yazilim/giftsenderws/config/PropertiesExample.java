package vip.yazilim.giftsenderws.config;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
/**
 * @author Burak Erkan - burak@yazilim.vip
 * 03 February 2022
 *
 * ref: https://mkyong.com/spring-boot/spring-boot-profile-based-properties-and-yaml-example/
 *
 * pom.xml plugin config : AnnotationProcessorPath Added
 *  spring-boot-configuration-processor
 */

@Component
@ConfigurationProperties(prefix = "vip-properties")
@Getter @Setter
@ToString
public class PropertiesExample {
	private String name;
	private String email;
	private List<Cluster> clusters = new ArrayList<>();

	@Getter @Setter
	@ToString
	public static class Cluster {
		private String ip;
		private String path;
	}
}