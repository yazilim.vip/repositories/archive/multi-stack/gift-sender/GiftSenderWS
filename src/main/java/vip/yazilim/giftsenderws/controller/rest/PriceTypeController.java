package vip.yazilim.giftsenderws.controller.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import vip.yazilim.giftsenderws.dto.mapper.PriceTypeMapper;
import vip.yazilim.giftsenderws.dto.dto.pricetype.PriceTypeRequestDto;
import vip.yazilim.giftsenderws.dto.dto.pricetype.PriceTypeSummaryDto;
import vip.yazilim.giftsenderws.entity.enums.LevelType;
import vip.yazilim.giftsenderws.model.RestResponse;
import vip.yazilim.giftsenderws.service.PriceTypeService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 31 January 2022
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("priceType")
@Slf4j
public class PriceTypeController {

	private final PriceTypeService priceTypeService;
	private final PriceTypeMapper priceTypeMapper;

	@PostMapping
	public RestResponse<PriceTypeSummaryDto> create(
			@RequestParam String levelTypeName,
			@RequestBody PriceTypeRequestDto priceTypeDto) {
		var priceTypeEntity = priceTypeMapper.toEntity(priceTypeDto);
		priceTypeEntity.setLevelType(LevelType.valueOf(levelTypeName));
		var data = priceTypeService.create(priceTypeEntity);
		var result = priceTypeMapper.toPriceTypeSummaryDto(data);
		return RestResponse.success(result);
	}

	@GetMapping
	public RestResponse<List<PriceTypeSummaryDto>> getAllPriceTypes() {
		var data = priceTypeService.getAllPriceTypes();
		var result = data.stream()
				.map(priceTypeMapper::toPriceTypeSummaryDto)
				.collect(Collectors.toList());
		return RestResponse.success(result);
	}

	@GetMapping("/levelTypes")
	public RestResponse<List<String>> getAllPriceTypeLevelTypes() {
		var result = priceTypeService.getAllPriceTypeLevelTypes();
		return RestResponse.success(result);
	}

	@GetMapping("/get/{level_type}")
	public RestResponse<PriceTypeSummaryDto> getPriceType(
			@PathVariable(name = "level_type") LevelType levelType) {
		return priceTypeService.findPriceTypeByLevelType(levelType)
				.map(priceTypeMapper::toPriceTypeSummaryDto)
				.map(RestResponse::success)
				.orElseGet(RestResponse::empty);
	}

	@PutMapping("/{levelType}")
	@ResponseStatus(HttpStatus.OK)
	public RestResponse<PriceTypeSummaryDto> update(
			@PathVariable LevelType levelType,
			@RequestBody PriceTypeRequestDto priceTypeDto) {
		var priceTypeEntity = priceTypeMapper.toEntity(priceTypeDto);
		priceTypeEntity.setLevelType(levelType);
		var data = priceTypeService.update(priceTypeEntity);
		var result = priceTypeMapper.toPriceTypeSummaryDto(data);
		return RestResponse.success(result);
	}

	@DeleteMapping("/{priceTypeLevelType}")
	public RestResponse<PriceTypeSummaryDto> delete(
			@PathVariable LevelType priceTypeLevelType) {
		priceTypeService.delete(priceTypeLevelType);
		return RestResponse.empty();
	}
}
