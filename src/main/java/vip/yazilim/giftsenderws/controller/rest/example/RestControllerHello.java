package vip.yazilim.giftsenderws.controller.rest.example;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Burak Erkan - 17 January 2022
 * burak@yazilim.vip
 */

@RequiredArgsConstructor
@RestController
@RequestMapping("/greeting")
@Slf4j
public class RestControllerHello {

	/*
	curl --request GET \
		--url http://localhost:8080/api/greeting \
		--header 'Content-Type: application/json'
	 */
	@GetMapping
	public String hello() {
		log.info("Log deneme");
		return "Selam olsun.";
	}

	@GetMapping("/test")
	public String testUncaughtException() {
		throw new RuntimeException();
	}
}
