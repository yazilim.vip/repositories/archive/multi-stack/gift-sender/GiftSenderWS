package vip.yazilim.giftsenderws.controller.rest.example;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import vip.yazilim.giftsenderws.entity.Person;
import vip.yazilim.giftsenderws.entity.Present;
import vip.yazilim.giftsenderws.service.example.PageAndSortExampleService;

import java.util.List;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 02 February 2022
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/example/pagesort")
@Slf4j
public class PageAndSortExampleController {

	private final PageAndSortExampleService service;

	@GetMapping("/person/{numberOfPages}")
	public List<Person> pageExample(@PathVariable Integer numberOfPages){
		var data = service.getPersonList_OnePage(numberOfPages);
		return data;
	}

	@GetMapping("/present/{numberOfPage}/{numberOfPageElements}")
	public List<Present> pageAndSortExample(@PathVariable Integer numberOfPage
			,@PathVariable Integer numberOfPageElements){
		var data = service.getPresentList_SortedByPriceDescNameAsc(numberOfPage,numberOfPageElements);
		return data;
	}

	@GetMapping("/present")
	public Page<Present> pageableExample(
			@PageableDefault(page = 0, size = 10) Pageable pageable){
		var data = service.getPresentList_Pageable(pageable);
		return data;
	}

}
