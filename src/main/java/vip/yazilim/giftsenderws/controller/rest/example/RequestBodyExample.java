package vip.yazilim.giftsenderws.controller.rest.example;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.yazilim.giftsenderws.dto.dto.person.PersonSummaryDto;

/**
 * @author Burak Erkan - 17 January 2022
 * @contact burak@yazilim.vip
 *
 *  curl --request POST \
 *   --url http://localhost:8080/greeting/user \
 *   --header 'Content-Type: application/json' \
 *   --data '{
 * 	"fname" : "burak",
 * 	"lname" : "erkan",
 * 	"age" : 37
 *   }'
 */

@RestController
@RequestMapping("/greeting")
@Slf4j
public class RequestBodyExample {

	/**
	 * RequestBody Example
	 * TODO change user with person entity and delete user model
	 */
	@PostMapping("/welcome")
	public String helloRequestBodyExample(@RequestBody PersonSummaryDto personSummaryDto) {
		String welcomeStr = "Log deneme : " + personSummaryDto.getName() + ", " + personSummaryDto.getSurname();
		log.info("LOG: " + welcomeStr);
		return welcomeStr;
	}

}
