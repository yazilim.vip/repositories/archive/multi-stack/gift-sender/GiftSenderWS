package vip.yazilim.giftsenderws.controller.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import vip.yazilim.giftsenderws.dto.mapper.PresentMapper;
import vip.yazilim.giftsenderws.dto.dto.present.PresentRequestDto;
import vip.yazilim.giftsenderws.dto.dto.present.PresentSummaryDto;
import vip.yazilim.giftsenderws.model.RestResponse;
import vip.yazilim.giftsenderws.service.PresentService;

import java.rmi.NoSuchObjectException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 31 January 2022
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("present")
@Slf4j
public class PresentController {

	private final PresentService presentService;
	private final PresentMapper presentMapper;

	@PostMapping
	public RestResponse<PresentSummaryDto> create(
			@RequestBody PresentRequestDto presentDto) {

		var presentEntity = presentMapper.toEntity(presentDto);
		var data = presentService.create(presentEntity);
		var result = presentMapper.toPresentSummaryDto(data);

		return RestResponse.success(result);
	}

	@GetMapping("/{presentId}")
	public RestResponse<PresentSummaryDto> getPresent(
			@PathVariable Long presentId) {

		return presentService.findPresentById(presentId)
				.map(presentMapper::toPresentSummaryDto)
				.map(RestResponse::success)
				.orElseGet(RestResponse::empty);
	}

	@GetMapping
	public RestResponse<List<PresentSummaryDto>> getAllPresent() {

		var result = presentService.getAllPresent().stream()
				.map(presentMapper::toPresentSummaryDto)
				.collect(Collectors.toList());

		return RestResponse.success(result);
	}

	@PutMapping("/{presentId}")
	@ResponseStatus(HttpStatus.OK)
	public RestResponse<PresentSummaryDto> update(
			@PathVariable Long presentId,
			@RequestBody PresentRequestDto presentDto) throws NoSuchObjectException {

		var presentEntity = presentMapper.toEntity(presentDto);
		presentEntity.setId(presentId);
		var data = presentService.update(presentEntity);
		var result = presentMapper.toPresentSummaryDto(data);

		return RestResponse.success(result);
	}

	@DeleteMapping("/{presentId}")
	public RestResponse<PresentSummaryDto> delete(
			@PathVariable Long presentId) throws NoSuchObjectException {

		presentService.delete(presentId);

		return RestResponse.empty();
	}
}
