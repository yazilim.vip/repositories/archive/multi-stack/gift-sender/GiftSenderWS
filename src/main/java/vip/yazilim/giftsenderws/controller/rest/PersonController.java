package vip.yazilim.giftsenderws.controller.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import vip.yazilim.giftsenderws.dto.dto.person.PersonCreateRequestDto;
import vip.yazilim.giftsenderws.dto.dto.person.PersonSummaryDto;
import vip.yazilim.giftsenderws.dto.dto.person.PersonUpdateRequestDto;
import vip.yazilim.giftsenderws.dto.mapper.PersonMapper;
import vip.yazilim.giftsenderws.model.RestResponse;
import vip.yazilim.giftsenderws.service.PersonService;

import javax.validation.Valid;
import java.rmi.NoSuchObjectException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 20 January 2022
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/person")
@Slf4j
public class PersonController {

	private final PersonService personService;
	private final PersonMapper personMapper;

	@PostMapping
	public RestResponse<PersonSummaryDto> create(
			@Valid @RequestBody PersonCreateRequestDto personDto) {

		var personEntity = personMapper.toEntity(personDto);
		personEntity.setWantPresent(true);
		var data = personService.create(personEntity);
		var result = personMapper.toPersonSummaryDto(data);

		return RestResponse.success(result);
	}

	@GetMapping("/{personId}")
	public RestResponse<PersonSummaryDto> getPerson(
			@PathVariable Long personId) {

		return personService.findPersonById(personId)
				.map(personMapper::toPersonSummaryDto)
				.map(RestResponse::success)
				.orElseGet(RestResponse::empty);
	}

	@GetMapping
	public RestResponse<List<PersonSummaryDto>> getAllPersons() {
		var result = personService.getAllPersons().stream()
				.map(personMapper::toPersonSummaryDto)
				.collect(Collectors.toList());

		return RestResponse.success(result);
	}

	/**
	 * refs:
	 * https://medium.com/kodgemisi/spring-data-jpa-partial-update-782db3734ba
	 * https://www.baeldung.com/http-put-patch-difference-spring
	 * https://www.baeldung.com/spring-rest-json-patch
	 */
	@PutMapping("/{personId}")
	@ResponseStatus(HttpStatus.OK)
	public RestResponse<PersonSummaryDto> update(
			@PathVariable Long personId,
			@RequestBody PersonUpdateRequestDto personDto) throws NoSuchObjectException {

		var personEntity = personMapper.toEntity(personDto);
		personEntity.setId(personId);
		var data = personService.update(personEntity);
		var result = personMapper.toPersonSummaryDto(data);

		return RestResponse.success(result);
	}

	@DeleteMapping("/{personId}")
	public RestResponse<PersonSummaryDto> delete(
			@PathVariable Long personId) {

		personService.delete(personId);

		return RestResponse.empty();
	}
}
