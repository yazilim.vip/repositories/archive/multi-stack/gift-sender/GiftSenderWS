package vip.yazilim.giftsenderws.controller.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import vip.yazilim.giftsenderws.dto.mapper.CategoryMapper;
import vip.yazilim.giftsenderws.dto.dto.category.CategoryRequestDto;
import vip.yazilim.giftsenderws.dto.dto.category.CategorySummaryDto;
import vip.yazilim.giftsenderws.model.RestResponse;
import vip.yazilim.giftsenderws.service.CategoryService;

import java.rmi.NoSuchObjectException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 31 January 2022
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/category")
@Slf4j
public class CategoryController {

	private final CategoryService categoryService;
	private final CategoryMapper categoryMapper;

	@PostMapping
	public RestResponse<CategorySummaryDto> create(
			@RequestBody CategoryRequestDto categoryDto) {

		var categoryEntity = categoryMapper.toEntity(categoryDto);
		var data = categoryService.create(categoryEntity);
		var result = categoryMapper.toCategorySummaryDto(data);

		return RestResponse.success(result);
	}

	@GetMapping("/{categoryId}")
	public RestResponse<CategorySummaryDto> getCategory(
			@PathVariable Long categoryId) {

		return categoryService.findCategoryById(categoryId)
				.map(categoryMapper::toCategorySummaryDto)
				.map(RestResponse::success)
				.orElseGet(RestResponse::empty);
	}

	@GetMapping
	public RestResponse<List<CategorySummaryDto>> getAllCategorys() {

		var result = categoryService.getAllCategory().stream()
				.map(categoryMapper::toCategorySummaryDto)
				.collect(Collectors.toList());

		return RestResponse.success(result);
	}

	@PutMapping("/{categoryId}")
	@ResponseStatus(HttpStatus.OK)
	public RestResponse<CategorySummaryDto> update(
			@PathVariable Long categoryId,
			@RequestBody CategoryRequestDto categoryDto) throws NoSuchObjectException {

		var categoryEntity = categoryMapper.toEntity(categoryDto);
		categoryEntity.setId(categoryId);
		var data = categoryService.update(categoryEntity);
		var result = categoryMapper.toCategorySummaryDto(data);

		return RestResponse.success(result);
	}

	@DeleteMapping("/{categoryId}")
	public RestResponse<CategorySummaryDto> delete(
			@PathVariable Long categoryId) throws NoSuchObjectException {

		categoryService.delete(categoryId);

		return RestResponse.empty();
	}
}
