package vip.yazilim.giftsenderws.controller.rest.example;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.yazilim.giftsenderws.config.Messages;

import java.util.Locale;

/**
 * @author Burak Erkan - 17 January 2022
 * burak@yazilim.vip
 */
@RestController
@RequestMapping("/i18n")
@RequiredArgsConstructor
public class I18nByMessagesExample {

	@GetMapping
	public String hello(Locale locale) {

		var localizedGreetingStr = Messages
				.getMessageForLocale("message.greeting",locale);

		return localizedGreetingStr;
	}
}
