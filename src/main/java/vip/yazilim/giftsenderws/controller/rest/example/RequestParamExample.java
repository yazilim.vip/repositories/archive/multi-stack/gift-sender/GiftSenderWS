package vip.yazilim.giftsenderws.controller.rest.example;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Burak Erkan - 17 January 2022
 * @contact burak@yazilim.vip
 */

@RestController
@RequestMapping("/greeting")
@Slf4j
public class RequestParamExample {

	/**
	 * RequestParam Example
	 */
	@GetMapping("/user")
	public String helloRequestParamExample(@RequestParam(name = "welcome_msg") String greetingMsg) {
		String str = "Incoming RequestParam welcome_msg : " + greetingMsg;
		log.info("LOG: " + str);
		return str;
	}

}
