package vip.yazilim.giftsenderws.controller.rest.example;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Burak Erkan - 17 January 2022
 * @contact burak@yazilim.vip
 */

@RestController
@RequestMapping("/greeting")
@Slf4j
public class PathVariableExample {

	/*
	curl --request GET \
		--url http://localhost:8080/api/greeting/user/Burak Erk/47 \
		--header 'Content-Type: application/json'
	 */
	@GetMapping("/user/{welcome_msg}/{year}")
	public String helloPathVariableExample(
				@PathVariable("welcome_msg") String greetingMsg,
	            @PathVariable Integer year) {
		String str = "Incoming path variable : " + greetingMsg + ", year : " + year;
		log.info("LOG: " + str);
		return str;
	}

}
