package vip.yazilim.giftsenderws.exception;
import vip.yazilim.giftsenderws.constant.ErrorCode;
/**
 * @author Burak Erkan, burak@yazilim.vip
 * 27.02.2022
 */

public class ResourceNotFoundException extends BaseServiceException {
    public ResourceNotFoundException(String message) {
        super(message, ErrorCode.Commons.RESOURCE_NOT_FOUND);
    }

    public ResourceNotFoundException() {
        super("Resource Not Found.", ErrorCode.Commons.RESOURCE_NOT_FOUND);
    }
}
