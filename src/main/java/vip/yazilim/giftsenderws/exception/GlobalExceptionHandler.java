package vip.yazilim.giftsenderws.exception;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import vip.yazilim.giftsenderws.constant.ErrorCode;
import vip.yazilim.giftsenderws.service.RestErrorResponseGeneratorService;

import java.util.HashMap;
import java.util.Map;
/**
 * @author Burak Erkan - burak@yazilim.vip
 * 11 February 2022
 *
 * ref: https://youtu.be/AVUGHioyPhQ?list=PLF6Gq7_LMTCfaMbMld2WKdWZZt9Q-GNF0&t=1375
 */

@ControllerAdvice
@Slf4j
@RequiredArgsConstructor
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	private final RestErrorResponseGeneratorService restErrorResponseGeneratorService;

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException exception,
			HttpHeaders headers,
			HttpStatus status,
			WebRequest request) {

		Map<String,String> errorMap = new HashMap<>();
		exception.getBindingResult().getAllErrors().forEach((error -> {
			String fieldName = ((FieldError) error).getField();
			String message = error.getDefaultMessage();
			errorMap.put(fieldName,message);
		}));

		return new ResponseEntity<Object>(errorMap,HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseEntity<Object> handleAllUncaughtException(
			Exception exception,
			WebRequest request) {

		log.error(exception.getMessage(), exception);
		var httpRequest = ((ServletWebRequest) request).getRequest();
		var body = restErrorResponseGeneratorService
				.getCustomizedErrorResponse(httpRequest, ErrorCode.Commons.UN_HANDLED_ERROR);

		return ResponseEntity
				.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.body(body);
	}

	@ExceptionHandler(BaseServiceException.class)
	public ResponseEntity<Object> handleServiceException(
			BaseServiceException exception,
			WebRequest request) {

		var errorCode = exception.getCode();
		var responseStatus = errorCode == ErrorCode.Commons.RESOURCE_NOT_FOUND
				? HttpStatus.NOT_FOUND
				: HttpStatus.INTERNAL_SERVER_ERROR;
		log.error(exception.getMessage(), exception);
		var httpRequest = ((ServletWebRequest) request).getRequest();
		var body = restErrorResponseGeneratorService
				.getCustomizedErrorResponse(httpRequest, exception);

		return ResponseEntity
				.status(responseStatus)
				.body(body);
	}
}
