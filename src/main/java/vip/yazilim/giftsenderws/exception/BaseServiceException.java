package vip.yazilim.giftsenderws.exception;
import lombok.Getter;
/**
 * @author Burak Erkan, burak@yazilim.vip
 * 27.02.2022
 */

@Getter
public class BaseServiceException extends RuntimeException {

    private final String code;

    protected BaseServiceException(String message, String code) {
        super(message);
        this.code = code;
    }

    protected BaseServiceException(String message, Throwable throwable, String code) {
        super();
        this.code = code;
    }
}