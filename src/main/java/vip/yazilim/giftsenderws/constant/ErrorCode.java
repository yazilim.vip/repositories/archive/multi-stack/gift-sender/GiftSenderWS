package vip.yazilim.giftsenderws.constant;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
/**
 * @author Burak Erkan, burak@yazilim.vip
 * 27.02.2022
 */

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ErrorCode {

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static final class Commons {
        public static final String UN_HANDLED_ERROR = "E100";
        public static final String RESOURCE_NOT_FOUND = "E404";
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static final class UserRelatedErrors {
        public static final String EMAIL_ALREADY_EXISTS = "E1001";
    }
}
