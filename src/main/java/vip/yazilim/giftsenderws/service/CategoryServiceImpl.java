package vip.yazilim.giftsenderws.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import vip.yazilim.giftsenderws.entity.Category;
import vip.yazilim.giftsenderws.repository.CategoryRepo;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 26 January 2022
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class CategoryServiceImpl implements CategoryService {

	private final CategoryRepo categoryRepo;

	@Override
	public Category create(Category categoryToCreate) {
		if(categoryToCreate == null) {
			throw new IllegalArgumentException("Category is null");
		}

		Long categoryToCreateId = categoryToCreate.getId();
		if(categoryToCreateId != null) {
			throw new IllegalArgumentException("Category Id value is not null");
		}

		var createdCategory = categoryRepo.save(categoryToCreate);
		return createdCategory;
	}

	@Override
	public Optional<Category> findCategoryById(Long categoryId) {
		return categoryRepo.findById(categoryId);
	}

	@Override
	public Category update(Category categoryToUpdate) {
		if (categoryToUpdate == null) {
			throw new IllegalArgumentException("Category is null");
		}

		Long categoryToUpdateId = categoryToUpdate.getId();
		if (categoryToUpdateId == null) {
			throw new IllegalArgumentException("Category Id value is null");
		}

		Optional<Category> opt = findCategoryById(categoryToUpdateId);
		if (opt.isEmpty()) {
			throw new RuntimeException("Not found. CategoryId : " + categoryToUpdate.getId());
		}

		var existingCategory = opt.get();
		existingCategory.setPriceTypeLevelType(categoryToUpdate.getPriceTypeLevelType());
		return categoryRepo.save(existingCategory);
	}

	@Override
	public List<Category> getAllCategory() {
		var data = categoryRepo.findAll();
		return CollectionUtils.isEmpty(data) ? Collections.emptyList() : data;
	}

	@Override
	public void delete(Long categoryId) {
		if(categoryId == null) {
			throw new IllegalArgumentException("CategoryId is null");
		}

		Optional.ofNullable(findCategoryById(categoryId))
				.orElseThrow(() -> new RuntimeException("Not found to delete. CategoryId : " +  categoryId))
				.ifPresent(categoryRepo::delete);
	}
}
