package vip.yazilim.giftsenderws.service;

import vip.yazilim.giftsenderws.entity.PriceType;
import vip.yazilim.giftsenderws.entity.enums.LevelType;

import java.util.List;
import java.util.Optional;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 01 February 2022
 */
public interface PriceTypeService {
	PriceType create(PriceType priceTypeToCreate);

	List<PriceType> getAllPriceTypes();

	List<String> getAllPriceTypeLevelTypes();

	Optional<PriceType> findPriceTypeByLevelType(LevelType levelType);

	PriceType update(PriceType priceType);

	void delete(LevelType priceTypeLevelType);
}
