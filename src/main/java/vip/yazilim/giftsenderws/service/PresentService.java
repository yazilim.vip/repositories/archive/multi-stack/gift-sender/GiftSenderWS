package vip.yazilim.giftsenderws.service;

import vip.yazilim.giftsenderws.entity.Present;

import java.util.List;
import java.util.Optional;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 27 January 2022
 */
public interface PresentService {
	public Present create(Present presentToCreate);

	public Optional<Present> findPresentById(Long presentId);

	public Present update(Present present);

	public List<Present> getAllPresent();

	public void delete(Long presentId);
}
