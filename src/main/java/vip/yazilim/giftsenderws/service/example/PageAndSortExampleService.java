package vip.yazilim.giftsenderws.service.example;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import vip.yazilim.giftsenderws.entity.Person;
import vip.yazilim.giftsenderws.entity.Present;
import vip.yazilim.giftsenderws.repository.PersonRepo;
import vip.yazilim.giftsenderws.repository.PresentRepo;

import java.util.Collections;
import java.util.List;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 02 February 2022
 *
 * ref: https://www.baeldung.com/spring-data-jpa-pagination-sorting
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class PageAndSortExampleService {

	private final PersonRepo personRepo;
	private final PresentRepo presentRepo;

	public List<Person> getPersonList_OnePage(Integer numberOfPageElements) {
		Pageable firstPageWithTwoElements = PageRequest.of(0, numberOfPageElements);
		var pages = personRepo.findAll(firstPageWithTwoElements);
		Page<String> map = pages.map(person -> person.getName());

		var data = pages.getContent();

		return CollectionUtils.isEmpty(data) ? Collections.emptyList() : data;
	}

	public List<Present> getPresentList_SortedByPriceDescNameAsc(Integer numberOfPage, Integer numberOfPageElements) {
		Pageable sortedByPriceDescNameAsc =
				PageRequest.of(
						numberOfPage
						, numberOfPageElements
						, Sort.by("name").ascending()
								.and(Sort.by("price").descending())
				);
		var pages = presentRepo.findAll(sortedByPriceDescNameAsc);
		var data = pages.getContent();

		return CollectionUtils.isEmpty(data) ? Collections.emptyList() : data;
	}
	public Page<Present> getPresentList_Pageable(Pageable pageable) {
		var pages = presentRepo.findAll(pageable);
		return pages;
	}

	/*
	EX1
	Servis controller'a page döndürür
	Contoller client'a page döndürür.

	EX2
	Custom repo method yazılacak ve pageination kullanılır

	EX3
	Client'dan pagable almak
	https://reflectoring.io/spring-boot-paging/

	 */

}
