package vip.yazilim.giftsenderws.service.example;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 03 February 2022
 *
 * ref: https://mkyong.com/spring-boot/spring-boot-profile-based-properties-and-yaml-example/
 */
//@SpringBootApplication
//@RequiredArgsConstructor
//public class AccessingYamlPropertiesExample implements CommandLineRunner {
//
//	private final PropertiesExample vipProperties;
//
//	public static void main(String[] args) {
//		SpringApplication app = new SpringApplication(AccessingYamlPropertiesExample.class);
//		app.run();
//	}
//
//	public void run(String... args) throws Exception {
//		System.out.println(vipProperties);
//	}
//}