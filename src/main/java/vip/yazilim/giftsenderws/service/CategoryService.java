package vip.yazilim.giftsenderws.service;

import vip.yazilim.giftsenderws.entity.Category;

import java.util.List;
import java.util.Optional;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 26 January 2022
 */
public interface CategoryService {
	public Category create(Category categoryToCreate);

	public Optional<Category> findCategoryById(Long categoryId);

	public Category update(Category category);

	public List<Category> getAllCategory();

	public void delete(Long categoryId);
}
