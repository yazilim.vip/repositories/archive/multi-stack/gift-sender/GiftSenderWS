package vip.yazilim.giftsenderws.service;

import vip.yazilim.giftsenderws.entity.Person;

import java.util.List;
import java.util.Optional;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 20 January 2022
 */
public interface PersonService {
	Person create(Person personToSave);

	Optional<Person> findPersonById(Long personId);

	List<Person> getAllPersons();

	Person update(Person person);

	void delete(Long personId);
}
