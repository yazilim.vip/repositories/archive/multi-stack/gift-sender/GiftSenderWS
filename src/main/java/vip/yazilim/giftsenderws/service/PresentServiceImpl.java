package vip.yazilim.giftsenderws.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import vip.yazilim.giftsenderws.entity.Present;
import vip.yazilim.giftsenderws.repository.PresentRepo;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 31 January 2022
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class PresentServiceImpl implements PresentService {

	private final PresentRepo presentRepo;

	@Override
	public Present create(Present presentToCreate) {
		if(presentToCreate == null) {
			throw new IllegalArgumentException("Present is null");
		}

		Long presentToCreateId = presentToCreate.getId();
		if(presentToCreateId != null) {
			throw new IllegalArgumentException("Present Id value is not null");
		}

		var createdPresent = presentRepo.save(presentToCreate);
		return createdPresent;
	}

	@Override
	public Optional<Present> findPresentById(Long presentId) {
		return presentRepo.findById(presentId);
	}

	@Override
	public Present update(Present presentToUpdate) {
		if (presentToUpdate == null) {
			throw new IllegalArgumentException("Present is null");
		}

		Long presentToUpdateId = presentToUpdate.getId();
		if (presentToUpdateId == null) {
			throw new IllegalArgumentException("Present Id value is null");
		}

		Optional<Present> opt = findPresentById(presentToUpdateId);
		if (opt.isEmpty()) {
			throw new RuntimeException("Not found to update. PresentId : " + presentToUpdate.getId());
		}

		var existingPresent = opt.get();
		existingPresent.setName(presentToUpdate.getName());
		existingPresent.setPrice(presentToUpdate.getPrice());
		existingPresent.setCategory(presentToUpdate.getCategory());
		return presentRepo.save(existingPresent);
	}

	@Override
	public List<Present> getAllPresent() {
		var data = presentRepo.findAll();
		return CollectionUtils.isEmpty(data) ? Collections.emptyList() : data;
	}

	@Override
	public void delete(Long presentId) {
		if(presentId == null) {
			throw new IllegalArgumentException("PresentId is null");
		}

		Optional.ofNullable(findPresentById(presentId))
				.orElseThrow(() -> new RuntimeException("Not found to delete. PresentId : " +  presentId));

		presentRepo.deleteById(presentId);
	}
}
