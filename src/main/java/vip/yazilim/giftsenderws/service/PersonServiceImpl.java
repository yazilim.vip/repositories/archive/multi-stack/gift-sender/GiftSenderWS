package vip.yazilim.giftsenderws.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import vip.yazilim.giftsenderws.entity.Person;
import vip.yazilim.giftsenderws.exception.ResourceNotFoundException;
import vip.yazilim.giftsenderws.repository.PersonRepo;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 20 January 2022
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class PersonServiceImpl implements PersonService {

	/*
	TODO: Neden final olmak zorunda?
	Answer : Search for "Spring Constructor Injection"
    */
	private final PersonRepo personRepo;

	@Override
	public Person create(Person personToCreate) {
		if(personToCreate == null) {
			throw new IllegalArgumentException("Person is null");
		}

		Long personToCreateId = personToCreate.getId();
		if(personToCreateId != null) {
			throw new IllegalArgumentException("Person Id value is not null");
		}

		var createdPerson = personRepo.save(personToCreate);
		log.info("Saved Person Entity Id : " + createdPerson);
		return createdPerson;
	}

	@Override
	public Optional<Person> findPersonById(Long personId) {
		return personRepo.findById(personId);
	}

	@Override
	public Person update(Person personToUpdate) {
		if (personToUpdate == null) {
			throw new IllegalArgumentException("Person is null");
		}

		Long personToUpdateId = personToUpdate.getId();
		if (personToUpdateId == null) {
			throw new IllegalArgumentException("Person Id value is null");
		}

		Optional<Person> opt = findPersonById(personToUpdateId);
		if (opt.isEmpty()) {
			throw new ResourceNotFoundException("Person not found. PersonId : " + personToUpdate.getId());
		}

		var existingPerson = opt.get();
		existingPerson.setName(personToUpdate.getName());
		existingPerson.setSurname(personToUpdate.getSurname());
		existingPerson.setGenderType(personToUpdate.getGenderType());
		existingPerson.setBirthPlace(personToUpdate.getBirthPlace());
		existingPerson.setBirthDate(personToUpdate.getBirthDate());
		existingPerson.setBirthTime(personToUpdate.getBirthTime());
		existingPerson.setWantPresent(personToUpdate.isWantPresent());

		return personRepo.save(existingPerson);
	}

	@Override
	public List<Person> getAllPersons() {
		var data = personRepo.findAll();
		return CollectionUtils.isEmpty(data) ? Collections.emptyList() : data;
	}

	@Override
	public void delete(Long personId) {
		if(personId == null) {
			throw new IllegalArgumentException("PersonId is null");
		}

		Optional.ofNullable(findPersonById(personId))
				.orElseThrow(() -> new RuntimeException("no user found to delete with id : " +  personId));

		personRepo.deleteById(personId);
	}
}
