package vip.yazilim.giftsenderws.service;
import vip.yazilim.giftsenderws.exception.BaseServiceException;
import vip.yazilim.giftsenderws.model.RestResponse;

import javax.servlet.http.HttpServletRequest;
/**
 * @author Burak Erkan, burak@yazilim.vip
 * 27.02.2022
 */

public interface RestErrorResponseGeneratorService {
    RestResponse<Object> getCustomizedErrorResponse(HttpServletRequest request, BaseServiceException exception);

    RestResponse<Object> getCustomizedErrorResponse(HttpServletRequest request, String errorCode);
}
