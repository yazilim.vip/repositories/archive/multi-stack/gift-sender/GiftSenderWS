package vip.yazilim.giftsenderws.service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import vip.yazilim.giftsenderws.exception.BaseServiceException;
import vip.yazilim.giftsenderws.model.RestResponse;

import javax.servlet.http.HttpServletRequest;
/**
 * @author Burak Erkan, burak@yazilim.vip
 * 27.02.2022
 */

@Service
@Slf4j
public class RestErrorResponseGeneratorServiceImpl implements RestErrorResponseGeneratorService{

    @Override
    public RestResponse<Object> getCustomizedErrorResponse(HttpServletRequest request, BaseServiceException exception) {
        var errorCode = exception.getCode();
        return getCustomizedErrorResponse(request,errorCode);
    }

    @Override
    public RestResponse<Object> getCustomizedErrorResponse(HttpServletRequest request, String errorCode) {
        var requestUri = request.getRequestURI();
        var locale = LocaleContextHolder.getLocale();
        var errorMessage = "localized-dada";
        var response = RestResponse.builder()
                .message(errorMessage)
                .data(errorCode)
                .build();
        response.setSuccess(false);
        return response;
    }
}
