package vip.yazilim.giftsenderws.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import vip.yazilim.giftsenderws.repository.PersonRepo;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 20 January 2022
 * ref: https://stackoverflow.com/a/41644743/15822809
 *
 * LOADING INITIAL DATA - WAY1
 */
@Configuration
@Slf4j
public class LoadData {

	@Bean
	CommandLineRunner initDatabase(PersonRepo personRepo) {
		return (args) -> {
//			log.info("Preloading : " + personRepo.save(
//				Person.builder()
//						.name("burak")
//						.surname("erkan")
//						.birthDate(LocalDate.parse("1985-01-01"))
//						.birthTime(LocalTime.parse("12:00"))
//						.genderType(GenderType.MALE)
//						.birthPlace("Malatya")
//						.mail("burak@yazilim.vip")
//						.wantPresent(true)
//						.build()));
//			log.info("Preloading : " + personRepo.save(
//				Person.builder()
//						.name("emre")
//						.surname("sen")
//						.birthDate(LocalDate.parse("1995-01-01"))
//						.birthTime(LocalTime.parse("11:00"))
//						.genderType(GenderType.MALE)
//						.birthPlace("Artvin")
//						.mail("emre@yazilim.vip")
//						.wantPresent(false)
//						.build()));
	};
	}
}



/** LOADING INITIAL DATA - WAY2
 * Move to the Person Service
 Ref: https://auth0.com/blog/spring-boot-java-tutorial-build-a-crud-api/

 public PersonServiceImpl(PersonRepo personRepo) {
 this.personRepo = personRepo;
 this.personRepo.saveAll(defaultItems());
 }

 private static List<Person> defaultItems() {
 return List.of(
 Person.builder()
 .name("burak")
 .surname("erkan")
 .birthDate(LocalDate.parse("1985-01-01"))
 .birthTime(LocalTime.parse("12:00"))
 .genderType(GenderType.MALE)
 .birthPlace("Malatya")
 .mail("burak@yazilim.vip")
 .wantPresent(true)
 .build(),
 Person.builder()
 .name("emre")
 .surname("sen")
 .birthDate(LocalDate.parse("1995-01-01"))
 .birthTime(LocalTime.parse("11:00"))
 .genderType(GenderType.MALE)
 .birthPlace("Bursa")
 .mail("emre@yazilim.vip")
 .wantPresent(false)
 .build()
 );
 }
 */