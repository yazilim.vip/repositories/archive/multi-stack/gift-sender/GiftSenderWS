package vip.yazilim.giftsenderws.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vip.yazilim.giftsenderws.entity.PriceType;
import vip.yazilim.giftsenderws.entity.enums.LevelType;
import vip.yazilim.giftsenderws.repository.PriceTypeRepo;

import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Burak Erkan - burak@yazilim.vip
 * 31 January 2022
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class PriceTypeServiceImpl implements PriceTypeService {

	private final PriceTypeRepo priceTypeRepo;

	@Override
	public PriceType create(PriceType priceTypeToCreate) {
		if(priceTypeToCreate == null) {
			throw new IllegalArgumentException("PriceType is null");
		}

		var levelType = priceTypeToCreate.getLevelType();
		if(levelType == null) {
			throw new IllegalArgumentException("PriceType LevelType value is null");
		}

		Optional.ofNullable(findPriceTypeByLevelType(levelType))
				.orElseThrow(() -> new IllegalStateException("PriceType exists with LevelType : " + levelType ));

		return priceTypeRepo.save(priceTypeToCreate);
	}

	@Override
	public List<PriceType> getAllPriceTypes() {
		return priceTypeRepo.findAll();
	}

	@Override
	public Optional<PriceType> findPriceTypeByLevelType(LevelType levelType) {
		return priceTypeRepo.findById(levelType);
	}

	@Override
	public PriceType update(PriceType priceTypeToUpdate) {
		if (priceTypeToUpdate == null) {
			throw new IllegalArgumentException("PriceType is null");
		}

		LevelType priceTypeLevelType = priceTypeToUpdate.getLevelType();
		if (priceTypeLevelType == null) {
			throw new IllegalArgumentException("PriceType LevelType value is null");
		}

		Optional<PriceType> opt = findPriceTypeByLevelType(priceTypeLevelType);
		if (opt.isEmpty()) {
			throw new RuntimeException("Not found to update. PriceType LevelType : " + priceTypeToUpdate.getLevelName());
		}

		var existingPriceType = opt.get();
		existingPriceType.setLevelName(priceTypeToUpdate.getLevelName());
		existingPriceType.setLowerLimit(priceTypeToUpdate.getLowerLimit());
		existingPriceType.setUpperLimit(priceTypeToUpdate.getUpperLimit());

		return priceTypeRepo.save(existingPriceType);
	}

	@Override
	public List<String> getAllPriceTypeLevelTypes() {
		return EnumSet.allOf(LevelType.class).stream()
				.map(LevelType::toString)
				.collect(Collectors.toList());
	}

	@Override
	public void delete(LevelType priceTypeLevelType) {
		if(priceTypeLevelType == null) {
			throw new IllegalArgumentException("PriceType LevelType is null");
		}

		Optional.ofNullable(findPriceTypeByLevelType(priceTypeLevelType))
				.orElseThrow(() -> new RuntimeException("Not found to delete. PriceType LevelType : " +  priceTypeLevelType));

		priceTypeRepo.deleteById(priceTypeLevelType);
	}
}
