package vip.yazilim.giftsenderws.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import vip.yazilim.giftsenderws.entity.enums.GenderType;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Set;

/**
 * @author Burak Erkan - 17 January 2022
 * burak@yazilim.vip
 */

@Entity
@Table(name = "gs_person")
@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
public class Person extends BaseEntity {
	@Column(nullable = false)
	@ToString.Include
	private String name;

	@Column(nullable = false)
	@ToString.Include
	private String surname;

	@Column(nullable = false)
	@ToString.Include
	private String mail;

	@Enumerated(EnumType.STRING)
	@Column(name = "gender_type")
	private GenderType genderType;

	@Column(name = "birth_place")
	private String birthPlace;

	@Column(name = "birth_date")
	private LocalDate birthDate;

	@Column(name = "birth_time")
	private LocalTime birthTime;

	@Column(name = "want_present")
	private boolean wantPresent;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(
			name = "gs_person_interested_category",
			joinColumns = @JoinColumn(name = "person_id"),
			inverseJoinColumns = @JoinColumn(name = "category_id")
	)
	private Set<Category> categorySet;
}