package vip.yazilim.giftsenderws.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author Burak Erkan - 17 January 2022
 * @contact burak@yazilim.vip
 */

@Entity
@Table(name = "gs_present")
@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
@ToString(callSuper = true)
@SuperBuilder
public class Present extends BaseEntity {
	@Column(nullable = false)
	private String name;

	@Column
	private BigDecimal price;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "category_id")
	private Category category;
}
