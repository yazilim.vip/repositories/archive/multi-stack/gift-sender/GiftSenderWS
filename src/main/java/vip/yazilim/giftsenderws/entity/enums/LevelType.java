package vip.yazilim.giftsenderws.entity.enums;

public enum LevelType {
    CHEAP, NORMAL, EXPENSIVE;
}