package vip.yazilim.giftsenderws.entity.enums;

/**
 * @author Burak Erkan - 17 January 2022
 * @contact burak@yazilim.vip
 */
public enum GenderType {
	FEMALE,
	MALE,
	OTHER
}
