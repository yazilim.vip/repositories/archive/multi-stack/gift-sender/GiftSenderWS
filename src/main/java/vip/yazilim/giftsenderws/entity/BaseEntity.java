package vip.yazilim.giftsenderws.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Burak Erkan - 17 January 2022
 * @contact burak@yazilim.vip
 *
 * https://thorben-janssen.com/lombok-hibernate-how-to-avoid-common-pitfalls/
 */
@MappedSuperclass
@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
@ToString(onlyExplicitlyIncluded = true)
@SuperBuilder
public class BaseEntity implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ToString.Include
	private Long id;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o instanceof BaseEntity) {
			BaseEntity that = (BaseEntity) o;
			return id != null && that.id != null && id.equals(that.id);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
