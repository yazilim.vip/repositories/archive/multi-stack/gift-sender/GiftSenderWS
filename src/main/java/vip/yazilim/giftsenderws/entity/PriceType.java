package vip.yazilim.giftsenderws.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;
import vip.yazilim.giftsenderws.entity.enums.LevelType;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author Burak Erkan - 17 January 2022
 * burak@yazilim.vip
 */

@Entity @Table(name = "gs_price_type")
@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
@EqualsAndHashCode @ToString(callSuper = true)
@SuperBuilder
public class PriceType {
	@Id
	@Enumerated(EnumType.STRING)
	@Column(name = "level_type")
	private LevelType levelType;

	@Column(name = "level_name")
	private String levelName; //Example names; Cheap, Normal, Expensive etc.

	@Column(name = "upper_limit")
	private BigDecimal upperLimit;

	@Column(name = "lower_limit")
	private BigDecimal lowerLimit;
}
