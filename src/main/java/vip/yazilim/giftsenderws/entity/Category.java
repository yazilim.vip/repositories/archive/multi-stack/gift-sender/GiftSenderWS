package vip.yazilim.giftsenderws.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import vip.yazilim.giftsenderws.entity.enums.LevelType;

import javax.persistence.*;
import java.util.Set;

/**
 * @author Burak Erkan - 17 January 2022
 * burak@yazilim.vip
 */

@Entity @Table(name = "gs_category")
@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
@ToString(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
public class Category extends BaseEntity {
	@Column(nullable = false)
	@ToString.Include
	private String name; //Car, Toy, Cosmetics, Motorbike, Bicycle

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "price_type_level_type")
	@ToString.Include
	private PriceType priceTypeLevelType;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "category")
	private Set<Present> presentSet;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "categorySet")
	private Set<Person> personSet;
}
